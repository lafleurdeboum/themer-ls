# themer-ls

A `dircolors` template for [themer](https://github.com/mjswensen/themer) to have
consistent colors in `ls`.

Note that the purpose of this template is to keep the meanings conveyed with
`ls` colors, just using the closest to what `themer` calls them. For instance
`ls --color` will still output directories in blue, but it will use the blue
that was defined in `themer`. 

## Why

Why ? Because having consistent colors in different CLI tools can be an
unfinishable cursed nightmare. Base16 tried to address it, and themer took the
spirit a little further. With themer, you can have consistent colors across
`kitty`, `vim` (including `vim-fugitive`, `vim-gutter` and such),
`vim-lightline`, `powerline-rs`, `firefox`, `ls` ... yes, really, `ls` ! Looking
forward the great harmonization of all things !

## How

`themer-ls` uses the defaults provided by `dircolors --print-database`, and
replaces its color codes by translations of the theme's colors from hex format
to xterm's 256 color escape sequences. For details on what `dircolor` codes
should hold, see [this stackoverflow question](https://unix.stackexchange.com/questions/94299/dircolors-modify-color-settings-globaly).

## Installation & usage

Install this module wherever you have `themer` installed:

    npm install themer-ls

Then pass `themer-ls` as a `-t` (`--template`) arg to `themer`:

    themer -c my-colors.js -t themer-ls -o gen

Installation instructions for the generated theme(s) will be included in
`<output dir>/README.md`.
