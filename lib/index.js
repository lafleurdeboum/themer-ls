// themer-ls
//
// A reminder of the codes used bu themer :
// ${colors.accent0}             // Red                  [Error]
// ${colors.accent1}             // Orange
// ${colors.accent2}             // Yellow               [Warning]
// ${colors.accent3}             // Green                [Success]
// ${colors.accent4}             // Cyan
// ${colors.accent5}             // Blue
// ${colors.accent6}             // Caret/cursor (?)

// ${colors.shade0}              // BG color
// ${colors.shade1}              // UI
// ${colors.shade2}              // UI text selection
// ${colors.shade3}              // UI code comments
// ${colors.shade4}              // UI (?)
// ${colors.shade5}              // UI (?)
// ${colors.shade6}              // Foreground text (?)
// ${colors.shade7}              // Foreground text (?)

const convert = require('color-convert');
// Copied from @themer/vim :
// HACK `color-convert`'s conversion to ANSI currently isn't that accurate for
// grays
const ansi256Colors = [];

for (let color = 0; color < 255; ++color) {
  const hexVal = convert.ansi256.hex(color);
  ansi256Colors.push(hexVal);
}

ansi256Colors.closest = function(hexVal) {
  const [r, g, b] = convert.hex.rgb(hexVal);
  let minDistance = Infinity;
  let index; // NOTE equals ansi code
  for (let i = 0; i < this.length; ++i) {
    const [otherR, otherG, otherB] = convert.hex.rgb(this[i]);
    const distance = (r - otherR) ** 2
      + (g - otherG) ** 2
      + (b - otherB) ** 2;
    if (distance < minDistance) {
      minDistance = distance;
      index = i;
    }
  }
  return index;
};

const gtermToCterm = hexVal => ansi256Colors.closest(hexVal);

// dircolors uses a weird format, for info see
// https://unix.stackexchange.com/questions/94299/dircolors-modify-color-settings-globaly

const renderTheme = (colors, isDark) => `
# Below are the color init strings for the basic file types.
# One can use codes for 256 or more colors supported by modern terminals.
# The default color codes use the capabilities of an 8 color terminal
# with some additional attributes as per the following codes:
# Attribute codes:
# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes:
# 38;5;${gtermToCterm(colors.shade6)}=black 38;5;${gtermToCterm(colors.accent0)}=red 38;5;${gtermToCterm(colors.accent3)}=green 38;5;${gtermToCterm(colors.accent2)}=yellow 38;5;${gtermToCterm(colors.accent5)}=blue 38;5;${gtermToCterm(colors.accent6)}=magenta 38;5;${gtermToCterm(colors.accent4)}=cyan 38;5;${gtermToCterm(colors.shade0)}=white
# Background color codes:
# 48;5;${gtermToCterm(colors.shade6)}=black 48;5;${gtermToCterm(colors.accent0)}=red 48;5;${gtermToCterm(colors.accent3)}=green 48;5;${gtermToCterm(colors.accent2)}=yellow 48;5;${gtermToCterm(colors.accent5)}=blue 48;5;${gtermToCterm(colors.accent6)}=magenta 48;5;${gtermToCterm(colors.accent4)}=cyan 48;5;${gtermToCterm(colors.shade0)}=white
#NORMAL 00 # no color code at all
#FILE 00 # regular file: use no color at all
RESET 0 # reset to "normal" color
DIR 01;38;5;${gtermToCterm(colors.accent5)} # directory
LINK 01;38;5;${gtermToCterm(colors.accent4)} # symbolic link. (If you set this to 'target' instead of a
 # numerical value, the color is as for the file pointed to.)
MULTIHARDLINK 00 # regular file with more than one link
FIFO 48;5;${gtermToCterm(colors.shade6)};38;5;${gtermToCterm(colors.accent2)} # pipe
SOCK 01;38;5;${gtermToCterm(colors.accent6)} # socket
DOOR 01;38;5;${gtermToCterm(colors.accent6)} # door
BLK 48;5;${gtermToCterm(colors.shade6)};38;5;${gtermToCterm(colors.accent2)};01 # block device driver
CHR 48;5;${gtermToCterm(colors.shade6)};38;5;${gtermToCterm(colors.accent2)};01 # character device driver
ORPHAN 48;5;${gtermToCterm(colors.shade6)};38;5;${gtermToCterm(colors.accent0)};01 # symlink to nonexistent file, or non-stat'able file ...
MISSING 00 # ... and the files they point to
SETUID 38;5;${gtermToCterm(colors.shade0)};48;5;${gtermToCterm(colors.accent0)} # file that is setuid (u+s)
SETGID 38;5;${gtermToCterm(colors.shade6)};48;5;${gtermToCterm(colors.accent2)} # file that is setgid (g+s)
CAPABILITY 38;5;${gtermToCterm(colors.shade6)};48;5;${gtermToCterm(colors.accent0)} # file with capability
STICKY_OTHER_WRITABLE 38;5;${gtermToCterm(colors.shade6)};48;5;${gtermToCterm(colors.accent3)} # dir that is sticky and other-writable (+t,o+w)
OTHER_WRITABLE 38;5;${gtermToCterm(colors.accent5)};48;5;${gtermToCterm(colors.accent3)} # dir that is other-writable (o+w) and not sticky
STICKY 38;5;${gtermToCterm(colors.shade0)};48;5;${gtermToCterm(colors.accent5)} # dir with the sticky bit set (+t) and not other-writable
# This is for files with execute permission:
EXEC 01;38;5;${gtermToCterm(colors.accent3)}
# List any file extensions like '.gz' or '.tar' that you would like ls
# to colorize below. Put the extension, a space, and the color init string.
# (and any comments you want to add after a '#')
# If you use DOS-style suffixes, you may want to uncomment the following:
#.cmd 01;38;5;${gtermToCterm(colors.accent3)} # executables (bright green)
#.exe 01;38;5;${gtermToCterm(colors.accent3)}
#.com 01;38;5;${gtermToCterm(colors.accent3)}
#.btm 01;38;5;${gtermToCterm(colors.accent3)}
#.bat 01;38;5;${gtermToCterm(colors.accent3)}
# Or if you want to colorize scripts even if they do not have the
# executable bit actually set.
#.sh 01;38;5;${gtermToCterm(colors.accent3)}
#.csh 01;38;5;${gtermToCterm(colors.accent3)}
 # archives or compressed (bright red)
.tar 01;38;5;${gtermToCterm(colors.accent0)}
.tgz 01;38;5;${gtermToCterm(colors.accent0)}
.arc 01;38;5;${gtermToCterm(colors.accent0)}
.arj 01;38;5;${gtermToCterm(colors.accent0)}
.taz 01;38;5;${gtermToCterm(colors.accent0)}
.lha 01;38;5;${gtermToCterm(colors.accent0)}
.lz4 01;38;5;${gtermToCterm(colors.accent0)}
.lzh 01;38;5;${gtermToCterm(colors.accent0)}
.lzma 01;38;5;${gtermToCterm(colors.accent0)}
.tlz 01;38;5;${gtermToCterm(colors.accent0)}
.txz 01;38;5;${gtermToCterm(colors.accent0)}
.tzo 01;38;5;${gtermToCterm(colors.accent0)}
.t7z 01;38;5;${gtermToCterm(colors.accent0)}
.zip 01;38;5;${gtermToCterm(colors.accent0)}
.z 01;38;5;${gtermToCterm(colors.accent0)}
.dz 01;38;5;${gtermToCterm(colors.accent0)}
.gz 01;38;5;${gtermToCterm(colors.accent0)}
.lrz 01;38;5;${gtermToCterm(colors.accent0)}
.lz 01;38;5;${gtermToCterm(colors.accent0)}
.lzo 01;38;5;${gtermToCterm(colors.accent0)}
.xz 01;38;5;${gtermToCterm(colors.accent0)}
.zst 01;38;5;${gtermToCterm(colors.accent0)}
.tzst 01;38;5;${gtermToCterm(colors.accent0)}
.bz2 01;38;5;${gtermToCterm(colors.accent0)}
.bz 01;38;5;${gtermToCterm(colors.accent0)}
.tbz 01;38;5;${gtermToCterm(colors.accent0)}
.tbz2 01;38;5;${gtermToCterm(colors.accent0)}
.tz 01;38;5;${gtermToCterm(colors.accent0)}
.deb 01;38;5;${gtermToCterm(colors.accent0)}
.rpm 01;38;5;${gtermToCterm(colors.accent0)}
.jar 01;38;5;${gtermToCterm(colors.accent0)}
.war 01;38;5;${gtermToCterm(colors.accent0)}
.ear 01;38;5;${gtermToCterm(colors.accent0)}
.sar 01;38;5;${gtermToCterm(colors.accent0)}
.rar 01;38;5;${gtermToCterm(colors.accent0)}
.alz 01;38;5;${gtermToCterm(colors.accent0)}
.ace 01;38;5;${gtermToCterm(colors.accent0)}
.zoo 01;38;5;${gtermToCterm(colors.accent0)}
.cpio 01;38;5;${gtermToCterm(colors.accent0)}
.7z 01;38;5;${gtermToCterm(colors.accent0)}
.rz 01;38;5;${gtermToCterm(colors.accent0)}
.cab 01;38;5;${gtermToCterm(colors.accent0)}
.wim 01;38;5;${gtermToCterm(colors.accent0)}
.swm 01;38;5;${gtermToCterm(colors.accent0)}
.dwm 01;38;5;${gtermToCterm(colors.accent0)}
.esd 01;38;5;${gtermToCterm(colors.accent0)}
# image formats
.jpg 01;38;5;${gtermToCterm(colors.accent6)}
.jpeg 01;38;5;${gtermToCterm(colors.accent6)}
.mjpg 01;38;5;${gtermToCterm(colors.accent6)}
.mjpeg 01;38;5;${gtermToCterm(colors.accent6)}
.gif 01;38;5;${gtermToCterm(colors.accent6)}
.bmp 01;38;5;${gtermToCterm(colors.accent6)}
.pbm 01;38;5;${gtermToCterm(colors.accent6)}
.pgm 01;38;5;${gtermToCterm(colors.accent6)}
.ppm 01;38;5;${gtermToCterm(colors.accent6)}
.tga 01;38;5;${gtermToCterm(colors.accent6)}
.xbm 01;38;5;${gtermToCterm(colors.accent6)}
.xpm 01;38;5;${gtermToCterm(colors.accent6)}
.tif 01;38;5;${gtermToCterm(colors.accent6)}
.tiff 01;38;5;${gtermToCterm(colors.accent6)}
.png 01;38;5;${gtermToCterm(colors.accent6)}
.svg 01;38;5;${gtermToCterm(colors.accent6)}
.svgz 01;38;5;${gtermToCterm(colors.accent6)}
.mng 01;38;5;${gtermToCterm(colors.accent6)}
.pcx 01;38;5;${gtermToCterm(colors.accent6)}
.mov 01;38;5;${gtermToCterm(colors.accent6)}
.mpg 01;38;5;${gtermToCterm(colors.accent6)}
.mpeg 01;38;5;${gtermToCterm(colors.accent6)}
.m2v 01;38;5;${gtermToCterm(colors.accent6)}
.mkv 01;38;5;${gtermToCterm(colors.accent6)}
.webm 01;38;5;${gtermToCterm(colors.accent6)}
.webp 01;38;5;${gtermToCterm(colors.accent6)}
.ogm 01;38;5;${gtermToCterm(colors.accent6)}
.mp4 01;38;5;${gtermToCterm(colors.accent6)}
.m4v 01;38;5;${gtermToCterm(colors.accent6)}
.mp4v 01;38;5;${gtermToCterm(colors.accent6)}
.vob 01;38;5;${gtermToCterm(colors.accent6)}
.qt 01;38;5;${gtermToCterm(colors.accent6)}
.nuv 01;38;5;${gtermToCterm(colors.accent6)}
.wmv 01;38;5;${gtermToCterm(colors.accent6)}
.asf 01;38;5;${gtermToCterm(colors.accent6)}
.rm 01;38;5;${gtermToCterm(colors.accent6)}
.rmvb 01;38;5;${gtermToCterm(colors.accent6)}
.flc 01;38;5;${gtermToCterm(colors.accent6)}
.avi 01;38;5;${gtermToCterm(colors.accent6)}
.fli 01;38;5;${gtermToCterm(colors.accent6)}
.flv 01;38;5;${gtermToCterm(colors.accent6)}
.gl 01;38;5;${gtermToCterm(colors.accent6)}
.dl 01;38;5;${gtermToCterm(colors.accent6)}
.xcf 01;38;5;${gtermToCterm(colors.accent6)}
.xwd 01;38;5;${gtermToCterm(colors.accent6)}
.yuv 01;38;5;${gtermToCterm(colors.accent6)}
.cgm 01;38;5;${gtermToCterm(colors.accent6)}
.emf 01;38;5;${gtermToCterm(colors.accent6)}
# https://wiki.xiph.org/MIME_Types_and_File_Extensions
.ogv 01;38;5;${gtermToCterm(colors.accent6)}
.ogx 01;38;5;${gtermToCterm(colors.accent6)}
# audio formats
.aac 00;38;5;${gtermToCterm(colors.accent4)}
.au 00;38;5;${gtermToCterm(colors.accent4)}
.flac 00;38;5;${gtermToCterm(colors.accent4)}
.m4a 00;38;5;${gtermToCterm(colors.accent4)}
.mid 00;38;5;${gtermToCterm(colors.accent4)}
.midi 00;38;5;${gtermToCterm(colors.accent4)}
.mka 00;38;5;${gtermToCterm(colors.accent4)}
.mp3 00;38;5;${gtermToCterm(colors.accent4)}
.mpc 00;38;5;${gtermToCterm(colors.accent4)}
.ogg 00;38;5;${gtermToCterm(colors.accent4)}
.ra 00;38;5;${gtermToCterm(colors.accent4)}
.wav 00;38;5;${gtermToCterm(colors.accent4)}
# https://wiki.xiph.org/MIME_Types_and_File_Extensions
.oga 00;38;5;${gtermToCterm(colors.accent4)}
.opus 00;38;5;${gtermToCterm(colors.accent4)}
.spx 00;38;5;${gtermToCterm(colors.accent4)}
.xspf 00;38;5;${gtermToCterm(colors.accent4)}
`

const render = colors => Object.entries(colors).map(
  async ([name, colors]) => ({
    name: `themer-dircolors.conf`,
    contents: Buffer.from(renderTheme(colors, name === 'dark'), 'utf8'),
  })
);

const renderInstructions = paths =>
  "Use `dircolors` to set your `ls` colors :\n``` sh\ndircolors "
  + paths[0] + "\n```"

module.exports = {
  render,
  renderInstructions,
}

